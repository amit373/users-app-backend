import * as mongoose from 'mongoose';

export const photoSchema = new mongoose.Schema({
  url: { type: String, required: [true, 'Url is Required'] },
  isMain: { type: Boolean, default: false },
  description: { type: String },
  publicId: { type: String },
  createdAt: { type: Date, default: Date.now },
});

export class PhotoVM extends mongoose.Document {
  public url: string;
  public isMain: boolean;
  public publicId: string;
  public description: string;
  public createdAt?: Date | string;
}
