export interface ResonseVM {
  success: boolean;
  statusCode: number;
  message: string;
  data?: any | unknown;
}
