import {
  ExceptionFilter,
  Catch,
  ArgumentsHost,
  Logger,
  HttpException,
} from '@nestjs/common';
import { HttpArgumentsHost } from '@nestjs/common/interfaces';

@Catch()
export class HttpErrorFilter implements ExceptionFilter {
  catch(exception: HttpException, host: ArgumentsHost): void {
    const ctx: HttpArgumentsHost = host.switchToHttp();
    const response: any = ctx.getResponse();
    const request: any = ctx.getRequest();
    const status: number = exception.getStatus ? exception.getStatus() : 500;
    const errorResponse: ErrorResponse = {
      success: `${status}`.startsWith('20') ? true : false,
      statusCode: status,
      // method: request.method,
      // path: request.url,
      message: exception.message,
      // timestamp: new Date().toISOString(),
    };
    if (exception.name === 'ValidationError') {
      const { message, code } = this.handleValidationErrorDB(exception);
      errorResponse.statusCode = code;
      errorResponse.message = message;
    }

    if (exception.name === 'CastError') {
      const { message, code } = this.handleCastErrorDB(exception);
      errorResponse.statusCode = code;
      errorResponse.message = message;
    }

    if (exception.name === 'JsonWebTokenError') {
      const { message, code } = this.handleJWTError();
      errorResponse.statusCode = code;
      errorResponse.message = message;
    }

    if (exception.name === 'TokenExpiredError') {
      const { message, code } = this.handleJWTExpiredError();
      errorResponse.statusCode = code;
      errorResponse.message = message;
    }

    if ((exception as any).code === 11000) {
      const { message, code } = this.handleDuplicateFieldsDB(exception);
      errorResponse.statusCode = code;
      errorResponse.message = message;
    }

    if (status === 500) {
      Logger.error(
        `${request.method} ${request.url}`,
        exception.message,
        'ExceptionFilter',
      );
    } else {
      Logger.error(
        `${request.method} ${request.url}`,
        JSON.stringify(errorResponse),
        'ExceptionFilter',
      );
    }

    response.status(status).json(errorResponse);
  }

  handleValidationErrorDB(err) {
    const errors = Object.values(err.errors).map((el: any) => el.message);
    const message = `Invalid input data. ${errors.join('. ')}`;
    return { message, code: 400 };
  }

  handleCastErrorDB(err) {
    const message = `Invalid ${err.path}: ${err.value}.`;
    return { message, code: 400 };
  }

  handleDuplicateFieldsDB(err) {
    const value = err.errmsg.match(/(["'])(\\?.)*?\1/)[0];
    const message = `Duplicate field value: ${value}. Please use another value!`;
    return { message, code: 400 };
  }

  handleJWTError() {
    return { message: 'Invalid token. Please log in again!', code: 401 };
  }

  handleJWTExpiredError() {
    return {
      message: 'Your token has expired! Please log in again.',
      code: 401,
    };
  }
}

export interface ErrorResponse {
  success: boolean;
  statusCode: number;
  method?: string;
  path?: string;
  message: string;
  timestamp?: string | Date;
}
