export const ApiResponse = (
  success: boolean,
  statusCode: number,
  message: string,
  data?: any,
) => {
  const errorObj = {
    success,
    statusCode,
    message,
    data,
  };
  if (!data) {
    delete errorObj.data;
  }
  return errorObj;
};
