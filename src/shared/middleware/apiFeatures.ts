import { Query } from 'mongoose';

export class APIFeatures {
  query: Query<any>;
  queryString: any;
  constructor(query: any, queryString: any) {
    this.query = query;
    this.queryString = queryString;
  }

  public filter() {
    const queryObj: any = { ...this.queryString };
    const excludedFields: string[] = ['page', 'sort', 'limit', 'fields'];
    excludedFields.forEach((element: string) => delete queryObj[element]);
    // 1) Advanced filtering
    let queryStr: string = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, match => `$${match}`);
    this.query = this.query.find(JSON.parse(queryStr));
    return this;
  }

  public sort() {
    if (this.queryString.sort) {
      const sortBy: any = this.queryString.sort.split(',').join(' ');
      this.query = this.query.sort(sortBy);
    } else {
      this.query = this.query.sort('-createdAt');
    }
    return this;
  }

  public limitFields() {
    if (this.queryString.fields) {
      const fields: any = this.queryString.fields.split(',').join(' ');
      this.query = this.query.select(fields);
    } else {
      this.query = this.query.select('-__v');
    }
    return this;
  }

  public paginate() {
    // page=1&limit=4 page 1 => 4 records // page=2&limit=4 next four records
    const page: number = Number(this.queryString.page) * 1 || 1;
    const limit: number = Number(this.queryString.limit) * 1 || 100;
    const skip: number = (page - 1) * limit;
    this.query = this.query.skip(skip).limit(limit);
    return this;
  }
}
