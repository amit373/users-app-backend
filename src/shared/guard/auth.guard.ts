import {
  Injectable,
  CanActivate,
  ExecutionContext,
  HttpException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import { Model } from 'mongoose';
import * as jwt from 'jsonwebtoken';

import { UserVM } from 'src/auth/auth.model';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @InjectModel('User') private userModel: Model<UserVM>,
    private configService: ConfigService,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    if (request) {
      if (!request.headers.authorization) {
        throw new HttpException(
          'You are not logged in! Please log in to get access.',
          401,
        );
      }
      request.user = await this.validateToken(request.headers.authorization);
      return true;
    }
  }

  async validateToken(auth: string) {
    let token: any;
    let decoded: any = {};
    if (auth && auth.startsWith('Bearer')) {
      token = auth.split(' ')[1];
    }
    if (!token) {
      throw new HttpException(
        'You are not logged in! Please log in to get access.',
        401,
      );
    }

    // 2) Verification token
    try {
      decoded = jwt.verify(token, this.configService.get('JWT_SECRET'));
    } catch (err) {
      throw new HttpException(`Token Error ${err.message || err.name}`, 401);
    }

    // 3) Check if user still exists
    const currentUser: any = await this.userModel.findById(decoded.id);
    if (!currentUser) {
      throw new HttpException(
        'The user belonging to this token does no longer exist.',
        401,
      );
    }

    // 4) Check if user changed password after the token was issued
    if (currentUser.changedPasswordAfter(decoded.iat)) {
      throw new HttpException(
        'User recently changed password! Please log in again.',
        401,
      );
    }
    return decoded;
  }
}
