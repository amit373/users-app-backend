import {
  Controller,
  Get,
  Param,
  Put,
  Body,
  Delete,
  UseInterceptors,
  UploadedFile,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { FileInterceptor } from '@nestjs/platform-express';
import { HttpException } from '@nestjs/common';
import * as cloudinary from 'cloudinary';
import * as mongoose from 'mongoose';

import { UsersService } from './users.service';
import { UserVM } from './auth.model';
import { AuthGuard } from 'src/shared/guard/auth.guard';
import { PhotoVM } from './../photos/photos.model';
import { multerOptions } from './../shared/middleware/multer.config';
import { ApiResponse } from 'src/shared/middleware/appError';
@Controller('auth')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly configService: ConfigService,
  ) {
    cloudinary.v2.config({
      // eslint-disable-next-line @typescript-eslint/camelcase
      cloud_name: this.configService.get('CLOUDINARY_NAME'),
      // eslint-disable-next-line @typescript-eslint/camelcase
      api_key: this.configService.get('CLOUDINARY_API_KEY'),
      // eslint-disable-next-line @typescript-eslint/camelcase
      api_secret: this.configService.get('CLOUDINARY_API_SECRET'),
    });
  }

  // @desc      Get All Users
  // @route     GET /api/v1/auth/users
  // @access    Private
  @Get('/users')
  @UseGuards(AuthGuard)
  async getAllUser(@Query() query?: mongoose.Query<any>): Promise<any> {
    const users = await this.usersService.getAllUsers(query);
    return ApiResponse(true, 200, `Get All Users`, users);
  }

  // @desc      Get User
  // @route     GET /api/v1/auth/users/:id
  // @access    Private
  /**
   * @param  userId
   * @returns {UserVM}
   */
  @Get('/users/:id')
  @UseGuards(AuthGuard)
  async getUser(@Param('id') id: string): Promise<any> {
    const user: UserVM = await this.usersService.getUserById(id);
    return ApiResponse(true, 200, `Get User By Id: ${id}`, user);
  }

  // @desc      Update User
  // @route     PUT /api/v1/auth/users/:id
  // @access    Private
  /**
   * @param  userId
   * @returns {UserVM}
   */
  @Put('/users/:id')
  @UseGuards(AuthGuard)
  async updateUser(
    @Param('id') id: string,
    @Body() body: Partial<UserVM>,
  ): Promise<any> {
    const user: UserVM = await this.usersService.updateUser(id, body);
    return ApiResponse(true, 200, `User Updated By Id: ${id}`, user);
  }

  // @desc      Delete User
  // @route     DELETE /api/v1/auth/users/:id
  // @access    Private
  /**
   * @param  userId
   * @returns {null}
   */
  @Delete('/users/:id')
  @UseGuards(AuthGuard)
  async deleteUser(@Param('id') id: string): Promise<any> {
    await this.usersService.getUserById(id);
    return ApiResponse(true, 200, `Delete User By Id: ${id}`);
  }

  // @desc      Upload User Photo
  // @route     DELETE /api/v1/auth/users/:userId/photos   // @param file in formdata
  // @access    Private
  /**
   * @param  userId
   * @returns {PhotoVM}
   */
  @Post('users/:userId/photos')
  @UseGuards(AuthGuard)
  @UseInterceptors(FileInterceptor('file', multerOptions))
  async upload(
    @UploadedFile() file,
    @Param('userId') userId: string,
  ): Promise<any> {
    if (!file) {
      throw new HttpException('Please Provide File', 500);
    }
    try {
      const result: cloudinary.UploadApiResponse = await cloudinary.v2.uploader.upload(
        file.path,
        {
          transformation: { width: 500, height: 500, crop: 'fill' },
        },
      );
      const photo = {
        url: result.url,
        isMain: false,
        publicId: result.public_id,
        description: '',
        createdAt: result.created_at,
      };
      const createdPhoto: PhotoVM = await this.usersService.uploadPhoto(photo, userId);
      return ApiResponse(true, 200, `Photo Uploaded`, createdPhoto);
    } catch (error) {
      throw new HttpException(error.message, 500);
    }
  }

  // @desc      Remove User Photo
  // @route     DELETE /api/v1/auth/users/:userId/photos/photoId
  // @access    Private
  /**
   * @param  {userId, photoId}
   * @returns {PhotoVM}
   */
  @Delete('users/:userId/photos/:photoId')
  @UseGuards(AuthGuard)
  async deleteUserPhoto(
    @Param('userId') userId: string,
    @Param('photoId') photoId: string,
  ): Promise<any> {
    if (!userId && !photoId) {
      throw new HttpException('Please Provide UserId and PhotoId', 500);
    }
    await this.usersService.deleteUserPhotoByPhotoId(userId, photoId);
    return ApiResponse(true, 200, `Delete Photo By Id: ${photoId}`);
  }

  // @desc      Set Photo As Main
  // @route     DELETE /api/v1/auth/users/:userId/photos/:photoId/setMain
  // @access    Private
  /**
   * @param  {userId, userId}
   * @returns {PhotoVM}
   */
  @Post('users/:userId/photos/:photoId/setMain')
  @UseGuards(AuthGuard)
  async setPhotoMain(
    @Param('userId') userId: string,
    @Param('photoId') photoId: string,
  ): Promise<any> {
    if (!userId && !photoId) {
      throw new HttpException('Please Provide UserId and PhotoId', 500);
    }
    const photo: PhotoVM = await this.usersService.setPhotoMain(
      userId,
      photoId,
    );
    return ApiResponse(true, 200, `Photo Set As Main By Id: ${photoId}`, photo);
  }
}
