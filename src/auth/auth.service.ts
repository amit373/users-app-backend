import { Injectable, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';
import * as jwt from 'jsonwebtoken';
import { Model } from 'mongoose';

import { UserVM } from './auth.model';
@Injectable()
export class AuthService {
  constructor(
    @InjectModel('User') private userModel: Model<UserVM>,
    private configService: ConfigService,
  ) {}

  async register(userVM: UserVM): Promise<UserVM> {
    const { username } = userVM;
    const user = await this.userModel.findOne({ username });
    if (user) {
      throw new HttpException('User already exists', 500);
    }
    const createdUser = new this.userModel(userVM);
    await createdUser.save();
    return this.sanitizeUser(createdUser);
  }

  async login(userVM: UserVM): Promise<UserVM> {
    const { username, password } = userVM;
    const user: any = await this.userModel
      .findOne({ username })
      .select('username password photoUrl');
    if (!user) {
      throw new HttpException('Invalid credentials', 401);
    }
    // Check if password matches
    const isMatch = await user.matchPassword(password);
    if (isMatch) {
      return this.sanitizeUser(user);
    } else {
      throw new HttpException('Invalid credentials', 401);
    }
  }

  // Remove Password From Response
  private sanitizeUser(user: UserVM) {
    const sanitized = user.toObject();
    delete sanitized['password'];
    return sanitized;
  }

  // Get Token
  public async signToken(id: string): Promise<string> {
    return jwt.sign({ id }, this.configService.get('JWT_SECRET'), {
      expiresIn: this.configService.get('JWT_EXPIRES_IN'),
    });
  }

  // Get token from signToken Funtion, create cookie and send response
  public async sendTokenResponse(
    user: Partial<UserVM>,
  ): Promise<{
    success: boolean;
    token: string;
    username: string;
  }> {
    // Create token
    const token: string = await this.signToken(user._id);
    const { username } = user;

    // decode Token
    // const decoded: any = await jwt.verify(token, this.configService.get('JWT_SECRET'));
    // const { iat, exp } = decoded;

    // Remove password from output
    user.password = undefined;
    return { success: true, token, username };
  }
}
