import { Injectable, HttpException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import * as cloudinary from 'cloudinary';

import { UserVM } from './auth.model';
import { APIFeatures } from 'src/shared/middleware/apiFeatures';
import { PhotoVM } from 'src/photos/photos.model';
@Injectable()
export class UsersService {
  constructor(
    @InjectModel('User') private userModel: Model<UserVM>,
    @InjectModel('photos') private photosModel: Model<PhotoVM>,
  ) { }

  // Get Users
  async getAllUsers(query): Promise<UserVM[]> {
    query.fields =
      '_id,username,dateOfBirth,gender,knownAs,lastActive,city,country,country,photoUrl,createdAt';
    const features: APIFeatures = new APIFeatures(
      this.userModel.find({}),
      query,
    )
      .filter()
      .sort()
      .limitFields()
      .paginate();
    const users: UserVM[] = await features.query;
    if (!users) {
      throw new HttpException('No Users Found', 404);
    }
    return users;
  }

  // Get User
  async getUserById(id: string): Promise<UserVM> {
    const user: UserVM = await this.userModel
      .findById(id)
      .select('-__v')
      .populate('photos', '_id url description isMain createdAt');
    if (!user) {
      throw new HttpException('User Not Found', 404);
    }
    return user;
  }

  // Update User
  async updateUser(id: string, body: Partial<UserVM>): Promise<UserVM> {
    const user: UserVM = await this.userModel.findById(id);
    if (!user) {
      throw new HttpException('User Not Found', 404);
    }
    body.updatedAt = new Date().toISOString();
    return await this.userModel.findByIdAndUpdate(id, body, {
      new: true,
      runValidators: true,
    });
  }

  // Delete User
  async deleteUserById(id: string): Promise<UserVM> {
    if (!id) {
      throw new HttpException('User Not Found', 404);
    }
    const user = await this.userModel.findById(id);
    if (!user) {
      throw new HttpException('User Not Found', 404);
    }
    return await this.userModel.findByIdAndDelete(id);
  }

  // Upload Single Photo
  async uploadPhoto(photo: any, userId: string): Promise<PhotoVM> {
    const createdPhoto: PhotoVM = new this.photosModel(photo);
    await createdPhoto.save();
    await this.userModel.updateOne(
      { _id: userId },
      { $push: { photos: createdPhoto } },
    );
    delete createdPhoto.__v;
    delete createdPhoto.publicId;
    return createdPhoto;
  }

  // Delete Photo
  async deleteUserPhotoByPhotoId(
    userId: string,
    photoId: string,
  ): Promise<PhotoVM> {
    if (!userId && !photoId) {
      throw new HttpException('Please Provide UserId and PhotoId', 500);
    }
    // 1. Find Photo By Id to check it exist or not.
    const photo: PhotoVM = await this.photosModel.findById(photoId);
    if (!photo) {
      throw new HttpException('Photo Not Found!', 404);
    }
    // 2. Remove From Photo Collection
    await this.photosModel.findByIdAndDelete(photo._id);

    // 3. Remove From User Collection
    const user: UserVM = await this.userModel.findById({ _id: userId });
    if (!user) {
      throw new HttpException('Your are not owner of this img.', 500);
    } else {
      await this.userModel.updateOne(
        { _id: userId },
        { photoUrl: null },
        { $pull: { photos: photo._id } },
      );
    }

    // 4. Remove From Cloudinary By public id
    try {
      if (photo.publicId) {
        await cloudinary.v2.uploader.destroy(photo.publicId);
      }
    } catch (error) {
      await this.uploadPhoto(photo, userId);
      throw new HttpException(error.message, 500);
    }
    delete photo.__v;
    delete photo.publicId;
    return photo;
  }

  async setPhotoMain(userId: string, photoId: string): Promise<PhotoVM> {
    // 1. Find Photo By Id to check it exist or not.
    const photo: PhotoVM = await this.photosModel.findById(photoId);
    if (!photo) {
      throw new HttpException('Photo Not Found!', 404);
    }
    // 2. Update ALl Photos isMain false
    await this.photosModel.updateMany({}, { isMain: false });

    // 3. Mark Photo By PhotoId as True
    await this.photosModel.updateOne({ _id: photoId }, { isMain: true });
    photo.isMain = true;

    // 4. find userObj by userId and update photoUrl on it.
    const user: UserVM = await this.userModel.findById({ _id: userId });
    if (!user) {
      throw new HttpException('Your are not owner of this img.', 500);
    } else {
      await this.userModel.updateOne({ _id: userId }, { photoUrl: photo.url });
    }
    delete photo.__v;
    delete photo.publicId;
    return photo;
  }
}
