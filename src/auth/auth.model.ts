import * as bcrypt from 'bcryptjs';
import * as mongoose from 'mongoose';
import * as crypto from 'crypto';

import { PhotoVM } from 'src/photos/photos.model';

export const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: [true, 'username is Required'],
    unique: true,
    trim: true,
  },
  password: {
    type: String,
    required: [true, 'Password is Required'],
    minlength: 6,
    select: false,
  },
  gender: {
    type: String,
    required: [true, 'Gender is Required'],
    enum: ['male', 'female'],
  },
  dateOfBirth: { type: Date, required: [true, 'DateOfBirth is Required'] },
  knownAs: { type: String, required: [true, 'KnowAs is Required'] },
  lastActive: { type: Date, default: Date.now },
  introduction: { type: String, trim: true },
  lookingFor: { type: String, trim: true },
  interests: { type: String, trim: true },
  city: { type: String, required: [true, 'City is Required'] },
  country: { type: String, required: [true, 'Country is Required'] },
  photoUrl: { type: String },
  photos: [{ type: mongoose.Schema.Types.ObjectId, ref: 'photos' }],
  passwordChangedAt: Date,
  resetPasswordToken: String,
  resetPasswordExpire: Date,
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date },
});

// Hash Password
userSchema.pre('save', async function(next: mongoose.HookNextFunction) {
  try {
    if (!this.isModified('password')) {
      return next();
    }
    const hashed = await bcrypt.hash(this['password'], 10);
    this['password'] = hashed;
    return next();
  } catch (err) {
    return next(err);
  }
});

// To Check is Password Changed
userSchema.pre('save', function(next) {
  if (!this.isModified('password') || this.isNew) return next();
  this['passwordChangedAt'] = Date.now() - 1000;
  next();
});

// For Set Invalid Current Token if Password Changed
userSchema.methods.changedPasswordAfter = function(JWTTimestamp: any) {
  if (this.passwordChangedAt) {
    const time: any = this.passwordChangedAt.getTime() / 1000;
    const changedTimestamp: any = parseInt(time, 10);
    return JWTTimestamp < changedTimestamp;
  }
  // False means NOT changed
  return false;
};

// Compare password using bcrypt
userSchema.methods.matchPassword = async function(enteredPassword: string) {
  return await bcrypt.compare(enteredPassword, this.password);
};
// Generate and hash password token for reset Password
userSchema.methods.getResetPasswordToken = async function() {
  // Generate token
  const resetToken = await crypto.randomBytes(20).toString('hex');
  // Hash token and set to resetPasswordToken field
  this.resetPasswordToken = await crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');
  // Set expire
  this.resetPasswordExpire = Date.now() + 10 * 60 * 1000;
  return resetToken;
};

export class UserVM extends mongoose.Document {
  public username: string;
  public knownAs: string;
  public gender: string;
  public dateOfBirth: Date;
  public city: string;
  public country: string;
  public password: string;
  public lastActive?: Date;
  public introduction?: string;
  public lookingFor?: string;
  public interests?: string;
  public photoUrl?: string;
  public photos?: PhotoVM[];
  public createdAt?: Date;
  public updatedAt?: Date | string;
}

export interface AuthRspVM {
  success: boolean;
  statusCode: number;
  message: string;
  user: { _id: string; username: string; photoUrl: string };
  token: string;
}
