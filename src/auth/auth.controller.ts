import { Controller, Post, Body, UsePipes } from '@nestjs/common';
import * as Joi from '@hapi/joi';

import { AuthService } from './auth.service';
import { UserVM } from './auth.model';
import { JoiValidationPipe } from 'src/shared/validation/joiValidation.pipe';
import { AuthRspVM } from './auth.model';

const registerSchema: Joi.ObjectSchema<UserVM> = Joi.object({
  gender: Joi.string().required(),
  username: Joi.string()
    .required()
    .min(2),
  knownAs: Joi.string().required(),
  dateOfBirth: Joi.string().required(),
  city: Joi.string().required(),
  country: Joi.string().required(),
  password: Joi.string()
    .required()
    .min(6)
    .max(8),
  lastActive: Joi.string().optional(),
  introduction: Joi.string().optional(),
  lookingFor: Joi.string().optional(),
  interests: Joi.string().optional(),
});

const loginSchema: Joi.ObjectSchema<UserVM> = Joi.object({
  username: Joi.string()
    .required()
    .min(2),
  password: Joi.string()
    .required()
    .min(6)
    .max(8),
});

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  // @desc      Register User
  // @route     POST /api/v1/auth/register
  // @access    Public
  @Post('/register')
  @UsePipes(new JoiValidationPipe(registerSchema))
  async register(@Body() userVM: UserVM): Promise<AuthRspVM> {
    const user: UserVM = await this.authService.register(userVM);
    const { token } = await this.authService.sendTokenResponse(user);
    const { _id, username, photoUrl } = user;
    return {
      success: true,
      statusCode: 201,
      message: 'Register Successfully',
      user: { _id, username, photoUrl },
      token,
    };
  }

  // @desc      Login User
  // @route     POST /api/v1/auth/login
  // @access    Public
  @Post('/login')
  @UsePipes(new JoiValidationPipe(loginSchema))
  async login(@Body() userVM: UserVM): Promise<AuthRspVM> {
    const user: UserVM = await this.authService.login(userVM);
    const { token } = await this.authService.sendTokenResponse(user);
    const { _id, username, photoUrl } = user;
    return {
      success: true,
      statusCode: 200,
      message: 'Login Successfully',
      user: { _id, username, photoUrl },
      token,
    };
  }
}
