import { APP_FILTER, APP_INTERCEPTOR } from '@nestjs/core';
import { Module } from '@nestjs/common';

import { HttpErrorFilter } from './shared/middleware/http-error.filter';
import { LoggingInterceptor } from './shared/interceptor/logging.interceptor';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [AuthModule],
  providers: [
    { provide: APP_FILTER, useClass: HttpErrorFilter },
    { provide: APP_INTERCEPTOR, useClass: LoggingInterceptor },
  ],
  exports: [],
})
export class ApiModule {}
